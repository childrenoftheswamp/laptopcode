set(_CATKIN_CURRENT_PACKAGE "swampbot_mapping")
set(swampbot_mapping_VERSION "0.0.0")
set(swampbot_mapping_MAINTAINER "swampy <swampy@todo.todo>")
set(swampbot_mapping_PACKAGE_FORMAT "2")
set(swampbot_mapping_BUILD_DEPENDS )
set(swampbot_mapping_BUILD_EXPORT_DEPENDS )
set(swampbot_mapping_BUILDTOOL_DEPENDS "catkin")
set(swampbot_mapping_BUILDTOOL_EXPORT_DEPENDS )
set(swampbot_mapping_EXEC_DEPENDS )
set(swampbot_mapping_RUN_DEPENDS )
set(swampbot_mapping_TEST_DEPENDS )
set(swampbot_mapping_DOC_DEPENDS )
set(swampbot_mapping_URL_WEBSITE "")
set(swampbot_mapping_URL_BUGTRACKER "")
set(swampbot_mapping_URL_REPOSITORY "")
set(swampbot_mapping_DEPRECATED "")